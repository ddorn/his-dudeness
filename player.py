from math import sqrt
from collections import deque
from functools import lru_cache

import pygame
from graphalama.core import Widget
from graphalama.colors import ImageBrush
from graphalama.constants import STRETCH
from graphalama.maths import Pos
from graphalama.shadow import NoShadow

from constants import MAX_SPEED, ACCELERATION


ZERO_DIR = Pos(0, 0)
UP = Pos(0, -1)
DOWN = Pos(0, 1)
RIGHT = Pos(1, 0)
LEFT = Pos(-1, 0)


class Player(Widget):
    ACCEPT_KEYBOARD_INPUT = True

    def __init__(self, pos, path):
        super().__init__(pos=pos,
                         shape=(80, 60),
                         bg_color=ImageBrush(path, STRETCH),
                         shadow=NoShadow())
        size = pygame.display.get_surface().get_size()
        self.min_x = 0
        self.min_y = 0
        self.max_x = size[0] - 80
        self.max_y = size[1] - 60
        self.score = 0
        self.focus = True

        self.speed = Pos(0, 0)
        self.acceleration = Pos(0, 0)

        self.pos_hist = deque(maxlen=5)

    def internal_logic(self):
        super().pre_render_update()

        # we use pressed to better handle LEFT + RIGHT or when we go out of the pause
        pressed = pygame.key.get_pressed()
        ax = ay = 0
        if pressed[pygame.K_LEFT]:
            ax -= 1
        if pressed[pygame.K_RIGHT]:
            ax += 1
        if pressed[pygame.K_UP]:
            ay -= 1
        if pressed[pygame.K_DOWN]:
            ay += 1
        self.acceleration = Pos(ax, ay)

        self.speed += self.acceleration
        # clamp the speed to MAX_SPEED
        if self.speed.norm() > MAX_SPEED:
            self.speed *= MAX_SPEED / self.speed.norm()

        # Brake
        if self.acceleration.x == 0:
            self.speed = Pos(self.speed.x / 2, self.speed.y)
        if self.acceleration.y == 0:
            self.speed = Pos(self.speed.x, self.speed.y / 2)

        self.pos += self.speed

        # Stay in the screen
        self.pos = (
            min(self.max_x, max(self.min_x, self.x)),
            min(self.max_y, max(self.min_y, self.y))
        )

        self.pos_hist.append(self.topleft)

    def catch_coins(self, coins):
        collisions = self.absolute_rect.collidelistall([coin.absolute_rect for coin in coins])
        for i in reversed(collisions):
            coins.pop(i)
            self.score += 1

    def render(self, display):
        for i, pos in enumerate(self.pos_hist):
            display.blit(self.get_img_with_transparency(16 * (i+1)^2), pos)
        super().render(display)

    @lru_cache(maxsize=5)
    def get_img_with_transparency(self, transparency):
        img = self.background_image.copy()
        img.fill((255, 255, 255, transparency), None, pygame.BLEND_RGBA_MIN)

        return img


