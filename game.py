#!/bin/python

import pygame
from graphalama.widgets import Button, SimpleText, WidgetList
from graphalama.constants import CENTER, RAINBOW, BOTTOMRIGHT, LLAMA, TRANSPARENT, GREEN, Monokai, NICE_BLUE, PURPLE, YELLOW, RED, WHITE
from graphalama.colors import MultiGradient, Gradient
from graphalama.shapes import RoundedRect
from graphalama.maths import Pos
from graphalama.font import default_font

from player import Player
from menu import MenuState
from coins import Coins
from statechinery import State
from constants import MENU, LIGHT_GREY


class PauseState(State):
    FPS = 30

    def __init__(self, state_machine, game_state_paused: State):
        self.paused_game = game_state_paused

        size = Pos(state_machine.display.get_size())
        widgets = [
            SimpleText("Paused", (size.x // 2, size.y // 3), color=WHITE + (128,), font=default_font(120), anchor=CENTER),
            Button(text="Play",
                   function=lambda: self.state_machine.set_temp_state(lambda s: self.paused_game),
                   pos=size / 2 - (0, 65),
                   shape=RoundedRect((200, 50), 100),
                   color=LIGHT_GREY,
                   bg_color=Gradient(NICE_BLUE, PURPLE),
                   anchor=CENTER),
            Button(text="Menu",
                   function=lambda: self.state_machine.set_state(MENU),
                   shape=RoundedRect((200, 50), 100),
                   color=LIGHT_GREY,
                   bg_color=Gradient(GREEN, Monokai.GREEN),
                   pos=size / 2,
                   anchor=CENTER),
            Button(text="Quit",
                   function=state_machine.quit,
                   pos=size / 2 + (0, 65),
                   shape=RoundedRect((200, 50), 100),
                   color=LIGHT_GREY,
                   bg_color=Gradient(YELLOW, RED),
                   anchor=CENTER)
        ]
        super().__init__(state_machine, widgets)

    def draw_background(self, display):
        # We draw the game as it should be behind, but we never update it
        self.paused_game.render(display)


class GameState(State):
    def __init__(self, state_machine):
        size = Pos(state_machine.display.get_size())
        self.coins = Coins()
        self.player = Player(size / 2, "assets/players/llama.png")
        self.score = SimpleText(text=self.player.score,
                                pos=size - (100, 30),
                                color=MultiGradient(*RAINBOW),
                                anchor=BOTTOMRIGHT)
        widgets = [
            Button(text="||",
                   function=lambda: state_machine.set_temp_state(lambda sm: PauseState(sm, self)),
                   pos=size - (50, 30),
                   color=LLAMA,
                   bg_color=TRANSPARENT,
                   anchor=BOTTOMRIGHT),
            self.coins,
            self.score,
            self.player,
        ]

        super().__init__(state_machine, widgets)
        self.load_background('rainbow')

    def internal_logic(self):
        self.player.internal_logic()
        self.coins.spawn(self.player.score)
        self.player.catch_coins(self.coins)
        self.score.text = self.player.score
        self.score.invalidate()

    def draw_background(self, display):
        display.blit(self.background, (0, 0))

    def load_background(self, name):
        img = pygame.image.load(f"assets/backgrounds/background_{name}.jpg")
        size = self.state_machine.display.get_size()
        self.background = pygame.transform.scale(img, size)
