import pygame
from graphalama.widgets import WidgetList

class StateMachine:
    """
    A state machine that represents and manages the different screens of the app

    Every screen in the app would be a State (so there should be one for Menu, one for Settings etc...
    And each of those states can easily change the current State by calling `set_state(NEW_STATE)`.
    """

    def __init__(self, states: dict, initial_state):
        self.states = states
        self.state = initial_state
        self.display = pygame.display.set_mode(pygame.display.list_modes()[0])
        self.clock = pygame.time.Clock()
        self.running = True

        self.current_state = self.states[self.state](self)

    def quit(self):
        """Kill the app"""
        self.running = False

    def run(self):
        self.running = True
        while self.running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quit()
                else:
                    self.current_state.update(event)

            self.current_state.internal_logic()
            self.current_state.render(self.display)

            pygame.display.flip()
            self.clock.tick(self.current_state.FPS)

    def set_state(self, new_state_id):
        self.state = new_state_id
        # We instantiate the state class
        self.current_state = self.states[self.state](self)

    def set_temp_state(self, state):
        """
        Put the SM in the given state.

        state: A State class/callable. The benefts of this function is that it allow for easily adding runtime states that are used only once
        """
        self.state = None
        self.current_state = state(self)

class State:
    """
    Represent a State/Screen of the app

    To implement a new state, you need to override `draw_background`, `internal_logic`, and pass a list of widgets to the constructor, and everything else is taken care of.
    To change state, call `self.state_machine.set_state(STATE_ID)`
    """

    FPS = 60

    def __init__(self, state_machine, widgets=()):
        self.state_machine = state_machine
        self.widgets = WidgetList(widgets)

    def draw_background(self, display):
        display.fill((255, 165, 0))

    def update(self, event):
        self.widgets.update(event)

    def internal_logic(self):
        """Override it if your state has stuff to run once a frame, before rendering"""

    def render(self, display):
        self.draw_background(display)
        self.widgets.render(display)

