#!/bin/python

import pygame; pygame.init()

from game import GameState
from menu import MenuState
from params import SettingsState
from statechinery import State, StateMachine
from constants import MENU, GAME, SETTINGS

def main():
    StateMachine({
        MENU: MenuState,
        GAME: GameState,
        SETTINGS: SettingsState
    }, MENU).run()


if __name__ == "__main__":
    main()
    print("bye bye")
