import pygame
from graphalama.widgets import Button
from graphalama.shapes import RoundedRect
from graphalama.constants import CENTER, NICE_BLUE, PURPLE, YELLOW, RED, Monokai
from graphalama.colors import Gradient

from constants import GAME, SETTINGS
from statechinery import State


class MenuState(State):
    """The Menu screen"""

    FPS = 30

    def __init__(self, state_machine):

        size = state_machine.display.get_size()
        widgets = [
            Button(text="Play",
                   function=lambda: state_machine.set_state(GAME),
                   pos=(size[0] // 2, size[1] // 2 - 65),
                   shape=RoundedRect((200, 50), 100),
                   color=(240, 240, 240),
                   bg_color=Gradient(NICE_BLUE, PURPLE),
                   anchor=CENTER),
            Button(text="Settings",
                   function=lambda: state_machine.set_state(SETTINGS),
                   shape=RoundedRect((200, 50), 100),
                   color=Monokai.PINK,
                   bg_color=(200, 200, 200, 72),
                   pos=(size[0] // 2, size[1] // 2),
                   anchor=CENTER),
            Button(text="Quit",
                   function=state_machine.quit,
                   pos=(size[0] // 2, size[1] // 2 + 65),
                   shape=RoundedRect((200, 50), 100),
                   color=(240, 240, 240),
                   bg_color=Gradient(YELLOW, RED),
                   anchor=CENTER)
        ]

        super().__init__(state_machine, widgets)
        self.load_background('color')

    def draw_background(self, display):
        display.blit(self.background, (0, 0))

    def load_background(self, name):
        img = pygame.image.load(f"assets/backgrounds/background_{name}.jpg")
        size = self.state_machine.display.get_size()
        self.background = pygame.transform.scale(img, size)

